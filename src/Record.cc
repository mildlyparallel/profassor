#include <iostream>
#include <cassert>
#include <sstream>

#include "Record.hh"

Record::Record()
{
	reset();
}

Record::value_type Record::operator[](Value v) const
{
	return get(v);
}

Record::value_type &Record::operator[](Value v)
{
	return get(v);
}

void Record::inc(Value v, uint64_t x)
{
	size_t i = static_cast<size_t>(v);
	assert(i < Value::nr_values);

	m_values[i].u64 += x;
}

void Record::inc(Value v, double x)
{
	size_t i = static_cast<size_t>(v);
	assert(i < Value::nr_values);
	m_values[i].dbl += x;
}

Record::value_type Record::get(Value v) const
{
	size_t i = static_cast<size_t>(v);
	assert(i < Value::nr_values);
	return m_values[i];
}

Record::value_type &Record::get(Value v)
{
	size_t i = static_cast<size_t>(v);
	assert(i < Value::nr_values);
	return m_values[i];
}

Record::value_type Record::operator[](size_t i) const
{
	assert(i < Value::nr_values);
	return m_values[i];
}

Record::value_type &Record::operator[](size_t i)
{
	assert(i < Value::nr_values);
	return m_values[i];
}

const char *Record::get_name(size_t i)
{
	assert(i < Value::nr_values);
	return m_names[i];
}

const char *Record::get_name(Value entry) {
	size_t i = static_cast<size_t>(entry);
	assert(i < Value::nr_values);
	return m_names[i];
}

void Record::set_cpumask_bit(size_t cpu, bool value)
{
	if (cpu < m_cpuset.size())
		m_cpuset.set(cpu, value);
}

std::string Record::get_cpumask_string() const
{
	std::stringstream ss;

	size_t n = m_cpuset.size();
	bool seen_nonzero = false;
	for (size_t i = 0; i < n; i += 4) {
		int v = 0;
		for (size_t j = 0; j < 4; ++j)
			v |= m_cpuset[(n - i - 1) - j] << (3 - j);

		if (v)
			seen_nonzero = true;

		if (seen_nonzero)
			ss << std::hex << v;
	}

	if (!seen_nonzero)
		return "0";

	return ss.str();
}

void Record::reset()
{
	for (size_t i = 0; i < m_values.size(); ++i)
		m_values[i].u64 = 0;

	m_cpuset.reset();
}

