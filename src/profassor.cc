#include <iostream>
#include <chrono>
#include <thread>

#include <signal.h>

#include "Config.hh"
#include "Logger.hh"
#include "Process.hh"
#include "Poll.hh"
#include "Perf.hh"
#include "Utils.hh"
#include "ProcFS.hh"
#include "RuntimeOutput.hh"

int main(int argc, const char *argv[])
{
	Config config;
	config.parse_cmdline(argc, argv);

	logger.info("Starting command:", config.command_path, "agrs:", config.command_args);

	Poll poll;
	poll.create();
	poll.notify_on_timer(config.sample_interval);
	poll.notify_on_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT, SIGCHLD});

	Process process;
	process.set_file(config.command_path);
	process.set_args(config.command_args);
	pid_t pid = process.fork();

	logger.info("Watching process", pid);

	Perf perf;
	perf.start_listening(pid);

	ProcFS procfs;

	process.exec();

	Poll::Alerts alerts;

	Record record;
	RuntimeOutput output(config.runtime_output_file);

	auto start_time = std::chrono::steady_clock::now();
	bool interrupted = false;

	while (!alerts.have_signal(SIGCHLD)) {

		if (alerts.have_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT})) {
			if (interrupted) {
				logger.info("Interrupted, sending SIGKILL");
				process.stop(true);
			} else {
				logger.info("Interrupted, sending SIGINT");
				process.stop(false);
				interrupted = true;
			}
		}

		if (alerts.timer) {
			record.reset();

			perf.update(record);

			procfs.update();

			procfs.count_proc(pid, record);

			auto dt = std::chrono::steady_clock::now() - start_time;
			uint64_t ts = 
				std::chrono::duration_cast<std::chrono::milliseconds>(dt).count();
			record[Record::Timestamp].u64 = ts;

			output.write(record);
		}

		poll.wait(alerts);
	}

	logger.info("Recieved SIGCHLD");

	process.sync();

	perf.stop_listening();

	poll.close();

	return 0;
}

