#include <iostream>

#include "Topology.hh"

#include <hwloc.h>

Topology::Topology()
: m_topo(new hwloc_topology_t)
{
	hwloc_topology_init(m_topo.get()); 

	load();
}

Topology::~Topology()
{ 
	hwloc_topology_destroy(*m_topo);
}

void Topology::load_recursive(hwloc_obj_t obj, Processor p)
{
	if (obj->type == HWLOC_OBJ_NUMANODE) {
		p.numanode = obj->os_index;
	} else if (obj->type == HWLOC_OBJ_MACHINE) {
		p.machine = obj->os_index;
	} else if (obj->type == HWLOC_OBJ_PACKAGE) {
		p.package = obj->os_index;
	} else if (obj->type == HWLOC_OBJ_CORE) {
		p.core = obj->os_index;
	} else if (obj->type == HWLOC_OBJ_PU) {
		p.cpu = obj->os_index;

		assert(p.cpu < m_processors.size());
		m_processors[p.cpu] = p;
	}

	for (unsigned i = 0; i < obj->arity; i++) {
		load_recursive(obj->children[i], p);
	}
}

void Topology::load()
{
	hwloc_topology_load(*m_topo);

	m_counts.cpu = hwloc_get_nbobjs_by_type(*m_topo, HWLOC_OBJ_PU);
	m_counts.core = hwloc_get_nbobjs_by_type(*m_topo, HWLOC_OBJ_CORE);
	m_counts.package = hwloc_get_nbobjs_by_type(*m_topo, HWLOC_OBJ_PACKAGE);
	m_counts.machine = hwloc_get_nbobjs_by_type(*m_topo, HWLOC_OBJ_MACHINE);
	m_counts.numanode = hwloc_get_nbobjs_by_type(*m_topo, HWLOC_OBJ_NUMANODE);

	m_processors.resize(m_counts.cpu);

	Processor p;
	load_recursive(hwloc_get_root_obj(*m_topo), p);
}

