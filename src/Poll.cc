#include <cassert>
#include <iostream>
#include <system_error>
#include <cerrno>

#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/signalfd.h>
#include <sys/eventfd.h>
#include <sys/timerfd.h>

#include "Poll.hh"
#include "Utils.hh"

Poll::Poll()
: m_logger("poll")
, m_poll_fd(-1)
, m_signals_fd(-1)
, m_event_fd(-1)
, m_timer_fd(-1)
{ }

Poll::Poll(Poll &&other)
: m_logger("poll")
, m_poll_fd(other.m_poll_fd)
, m_signals_fd(other.m_signals_fd)
, m_event_fd(other.m_event_fd)
, m_timer_fd(other.m_timer_fd)
{
	other.m_poll_fd = -1;
	other.m_event_fd = -1;
	other.m_signals_fd = -1;
	other.m_timer_fd = -1;
}

Poll::~Poll()
{ 
	if (m_poll_fd != -1)
		::close(m_poll_fd);

	if (m_signals_fd != -1)
		::close(m_signals_fd);

	if (m_event_fd != -1)
		::close(m_event_fd);

	if (m_timer_fd != -1)
		::close(m_timer_fd);
}

Poll &Poll::operator=(Poll &&other)
{
	close();

	m_poll_fd = other.m_poll_fd;
	other.m_poll_fd = -1;

	m_signals_fd = other.m_signals_fd;
	other.m_signals_fd = -1;

	m_event_fd = other.m_event_fd;
	other.m_event_fd = -1;

	m_timer_fd = other.m_timer_fd;
	other.m_timer_fd = -1;

	return *this;
}

void Poll::create()
{
	assert(m_poll_fd == -1);
	m_poll_fd = epoll_create1(0);
	THROW_IF(m_poll_fd < 0);

	m_logger.debug("Created poll descriptor");
}

void Poll::close()
{
	if (m_poll_fd != -1) {
		THROW_IF(::close(m_poll_fd) < 0);
		m_poll_fd = -1;
	}

	if (m_signals_fd != -1) {
		THROW_IF(::close(m_signals_fd) < 0);
		m_signals_fd = -1;
	}

	if (m_event_fd != -1) {
		THROW_IF(::close(m_event_fd) < 0);
		m_event_fd = -1;
	}
}

void Poll::poll_ctl(
	int op,
	uint32_t events,
	int fd,
	Poll::userdata_type userdata
) {
	assert(m_poll_fd != -1);
	assert(fd >= 0);

	struct epoll_event ev = {};
	ev.events = reinterpret_cast<uint32_t>(events);
	ev.data.u64 = reinterpret_cast<uint64_t>(userdata);

	int rc = ::epoll_ctl(m_poll_fd, op, fd, &ev);
	THROW_IF(rc < 0);
}

void Poll::notify_on_events()
{
	assert(m_event_fd == -1);

	m_event_fd = eventfd(0, EFD_NONBLOCK);
	THROW_IF(m_event_fd < 0);

	add(IN, m_event_fd, Userdata::Event);

	m_logger.debug("Created events descriptor");
}

void Poll::notify_on_signals(std::initializer_list<uint32_t> signals)
{
	assert(m_signals_fd == -1);

	int rc = 0;

	sigset_t mask;
	rc = sigemptyset(&mask);
	THROW_IF(rc < 0);
	for (auto s : signals) {
		rc = sigaddset(&mask, s);
		THROW_IF(rc < 0);
	}

	rc = sigprocmask(SIG_BLOCK, &mask, NULL);
	THROW_IF(rc < 0);

	m_signals_fd = signalfd(m_signals_fd, &mask, SFD_NONBLOCK);
	THROW_IF(m_signals_fd < 0);

	add(IN, m_signals_fd, Userdata::Signal);

	m_logger.debug("Created signal descriptor");
}

void Poll::notify_on_timer(std::chrono::milliseconds interval)
{
	assert(m_timer_fd == -1);

	std::chrono::seconds it_sec = std::chrono::duration_cast<std::chrono::seconds>(interval);
	std::chrono::nanoseconds it_nsec = std::chrono::duration_cast<std::chrono::nanoseconds>(interval);
	it_nsec -= it_sec;

	struct itimerspec new_value = {
		.it_interval = {
			.tv_sec = it_sec.count(),
			.tv_nsec = it_nsec.count()
		},
		.it_value = {
			.tv_sec = it_sec.count(),
			.tv_nsec = it_nsec.count(),
		}
	};

	m_timer_fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK);
	THROW_IF(m_timer_fd < 0);

	int rc = timerfd_settime(m_timer_fd, 0, &new_value, nullptr);
	THROW_IF(rc < 0);

	add(IN, m_timer_fd, Userdata::Timer);

	m_logger.debug("Created timer descriptor for", interval.count(), "ms");
}

void Poll::add(uint32_t events, int fd, Poll::userdata_type userdata)
{
	poll_ctl(EPOLL_CTL_ADD, events, fd, userdata);
}

void Poll::modify(uint32_t events, int fd, Poll::userdata_type userdata)
{
	poll_ctl(EPOLL_CTL_MOD, events, fd, userdata);
}

void Poll::remove(int fd)
{
	poll_ctl(EPOLL_CTL_DEL, 0, fd, 0);
}

void Poll::wait()
{
	Alerts alerts;
	wait(alerts);
}

void Poll::wait(Poll::Alerts &alerts)
{
	assert(m_poll_fd != -1);

	struct epoll_event events[max_events] = {};

	alerts.reset();

	int nready = epoll_wait(m_poll_fd, events, max_events, -1);
	if (nready < 0 && errno == EINTR)
		return;
	THROW_IF(nready < 0);

	for (int i = 0; i < nready; ++i) {
		if (events[i].data.u64 == Userdata::Signal) {
			read_signals(alerts.signals);
			continue;
		}

		if (events[i].data.u64 == Userdata::Event) {
			alerts.events = read_events();
			continue;
		}

		if (events[i].data.u64 == Userdata::Timer) {
			alerts.timer = read_timer();
			continue;
		}

		alerts.userdata.push_back(reinterpret_cast<userdata_type>(events[i].data.u64));
	}

	m_logger.debug("Wakeup events:", alerts.events,
		"timers:", alerts.timer,
		"signals:", alerts.signals.size(),
		"userdata:", alerts.userdata.size()
	);
}

void Poll::notify()
{
	if (m_event_fd == -1)
		return;

	m_logger.debug("Notifying poll");

	uint64_t u = 1;
	int rc = 0;

	do {
		rc = ::write(m_event_fd, &u, sizeof(u));
		THROW_IF(rc < 0 && errno != EINTR);
	} while (rc != sizeof(u));
}

size_t Poll::read_events()
{
	assert(m_event_fd);

	uint64_t u = 0;
	size_t n = 0;

	while (read(m_event_fd, &u, sizeof(u))) {
		n++;
	}

	return n;
}

size_t Poll::read_signals(std::vector<uint32_t> &signals)
{
	assert(m_signals_fd);

	struct signalfd_siginfo fdsi;
	size_t n = 0;

	signals.clear();

	while (read(m_signals_fd, &fdsi, sizeof(struct signalfd_siginfo))) {
		signals.push_back(fdsi.ssi_signo);
		m_logger.debug("Recieved signal", fdsi.ssi_signo);
		n++;
	}

	return n;
}

size_t Poll::read_timer()
{
	assert(m_timer_fd);

	uint64_t u = 0;
	size_t n = 0;

	while (read(m_timer_fd, &u, sizeof(u))) {
		n++;
	}

	return n;
}

bool Poll::read(int fd, void *p, size_t s)
{
	assert(fd >= 0);
	assert(p);
	assert(s);

	ssize_t rc = 0;
	do {
		rc = ::read(fd, p, s);
		if (rc < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
			return false;
	} while (rc < 0 && errno == EINTR);

	THROW_IF(static_cast<size_t>(rc) != s);

	return true;
}

bool Poll::is_active() const
{
	return m_poll_fd != -1;
}


