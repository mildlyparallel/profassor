#include "Logger.hh"

Logger logger;

Logger::Logger(const std::string &name)
: m_name(name)
, m_level(logger.get_level())
{ }

void Logger::set_name(const std::string &name)
{
	m_name = name;
}

const std::string &Logger::get_name() const
{
	return m_name;
}

Logger::Level Logger::get_level() const
{
	return m_level;
}

void Logger::set_level(Logger::Level level)
{
	m_level = level;
}

