#include <cassert>

#include <Dashh.hh>

#include "Config.hh"
#include "Logger.hh"

#include "config.h"

void Config::parse_cmdline(int argc, const char *argv[])
{
	using namespace dashh;

	Command cmd;

	cmd.setVersion(VERSION_STR);

	cmd << Text("profassor -- Profile My Ass");

	cmd << "\nUsage:";
	cmd << Usage("[options] <CMD> [ARGS...]");

	cmd << "\nOptions:";

	uint64_t sample_interval_ms = 0;

	cmd << Option(sample_interval_ms, "-i, --interval", "ms", "Sample interval in milliseconds");

	cmd << Option(runtime_output_file, "-o, --output", "path", "Output CSV file");

	bool verbose = false;
	cmd << Flag(verbose, "-v, --verbose", "Verbose output");

	bool quiet = false;
	cmd << Flag(quiet, "-q, --quiet", "Disable all output, including error messages");

	bool debug = false;
	cmd << Flag(debug, "--debug", "Debug output");

	cmd << Flag(PageVersion(), "-V, --version", "Print version and exit");

	cmd << Flag(PageHelp(), "-h, -?, --help", "Print help (this message) and exit");

	cmd << Positional(command_path, "CMD", "Command executable", Type::Required);

	cmd << Positional(command_args, "ARGS...", "List of arguments", Type::Greedy);

	Dashh t(&cmd);
	t.parse(argc, argv);

	if (sample_interval_ms > 0)
		sample_interval = std::chrono::milliseconds(sample_interval_ms);

	logger.set_level(Logger::Warning);
	if (verbose)
		logger.set_level(Logger::Info);
	if (debug)
		logger.set_level(Logger::Debug);
	if (quiet)
		logger.set_level(Logger::Quiet);
}

