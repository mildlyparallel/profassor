#include <system_error>
#include <cassert>
#include <cerrno>
#include <iostream>
#include <stdexcept>

#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/perf_event.h>
#include <linux/hw_breakpoint.h>
#include <asm/unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "Perf.hh"
#include "Utils.hh"

static long perf_event_open(
	struct perf_event_attr *hw_event,
	pid_t pid,
	int cpu,
	int group_fd,
	unsigned long flags
) {
	return syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
}

Perf::Perf()
: m_next_event_id(0)
, m_logger("perf")
{
	for (auto &e : m_events)
		e.fd = -1;
}

Perf::~Perf()
{
	if (m_next_event_id == 0)
		return;

	if (m_events[0].fd != -1)
		::ioctl(m_events[0].fd, PERF_EVENT_IOC_DISABLE, PERF_IOC_FLAG_GROUP);

	for (auto &e : m_events)
		if (e.fd != -1)
			::close(e.fd);
}

void Perf::close()
{
	for (auto &e : m_events) {
		if (e.fd == -1)
			continue;

		int rc = ::close(e.fd);
		THROW_IF(rc < 0);
		e.fd = -1;
	}
}

void Perf::start_listening(int pid)
{
	if (nr_events == 0)
		return;

	assert(pid >= 0);

	for (auto e : Config::perf_events) {
		m_logger.debug("Opening perf event for", Record::get_name(e.name), "for pid", pid);
		if (add(pid, e.type, e.config))
			continue;

		m_logger.error("Perf event for", Record::get_name(e.name), "is not opened");
	}

	if (m_next_event_id == 0)
		throw std::runtime_error("None of perf events are opened.");

	reset();
	enable();

	m_logger.info("Monitoring", m_next_event_id - 1, "events for pid", pid);
}

void Perf::stop_listening()
{
	if (nr_events == 0)
		return;

	disable();

	close();

	m_next_event_id = 0;
}

bool Perf::add(int pid, uint32_t type, uint32_t config)
{
	struct perf_event_attr pe = {0};

	pe.type = type;
	pe.size = sizeof(struct perf_event_attr);
	pe.config = config;
	pe.disabled = 1;
	pe.inherit = 1;
	pe.exclude_kernel = 1;
	pe.read_format = PERF_FORMAT_GROUP | PERF_FORMAT_ID;

	int leader_fd = -1;
	if (m_next_event_id > 0)
		leader_fd = m_events[0].fd;

	int fd = ::perf_event_open(&pe, pid, -1, leader_fd, 0);
	if (fd == -1) {
		m_logger.error("Skipping perf event", type, config, "perf_event_open fails with errno", errno);
		return false;
	}

	unsigned long long id;
	::ioctl(fd, PERF_EVENT_IOC_ID, &id);

	m_logger.debug("Opened event, id:", id, "for pid", pid);

	m_events[m_next_event_id] = {fd, id, 0ull};

	m_next_event_id++;
	return true;
}

void Perf::reset()
{
	assert(m_next_event_id > 0);
	m_logger.debug("Calling reset");
	int rc = ::ioctl(m_events[0].fd, PERF_EVENT_IOC_RESET, PERF_IOC_FLAG_GROUP);
	THROW_IF(rc == -1);
}

void Perf::enable()
{
	assert(m_next_event_id > 0);
	m_logger.debug("Calling enable");
	int rc = ::ioctl(m_events[0].fd, PERF_EVENT_IOC_ENABLE, PERF_IOC_FLAG_GROUP);
	THROW_IF(rc == -1);
}

void Perf::disable()
{
	assert(m_next_event_id > 0);
	m_logger.debug("Calling disable");
	int rc = ::ioctl(m_events[0].fd, PERF_EVENT_IOC_DISABLE, PERF_IOC_FLAG_GROUP);
	THROW_IF(rc == -1);
}

void Perf::read()
{
	assert(m_next_event_id > 0);

	static char buf[4096];

	struct read_format {
		uint64_t nr;
		struct {
			uint64_t value;
			uint64_t id;
		} values[];
	};

	struct read_format* rf = (struct read_format*) buf;

	::read(m_events[0].fd, buf, sizeof(buf));

	for (size_t i = 0; i < rf->nr; i++) {
		for (size_t j = 0; j < nr_events; j++) {
			if (rf->values[i].id == m_events[j].id) {
				m_events[j].value = rf->values[i].value;
				break;
			}
		}
	}
}

void Perf::update(Record &stats)
{
	if (nr_events == 0)
		return;

	assert(m_next_event_id > 0);

	read();

	size_t i = 0;

	for (auto &e : Config::perf_events) {
		stats[e.name].u64 = m_events[i].value;
		i++;
	}
}

