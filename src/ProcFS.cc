#include <cassert>
#include <fstream>
#include <optional>

#include "ProcFS.hh"

namespace fs = std::filesystem;

ProcFS::ProcFS()
: m_logger("procfs")
{
	m_cputime_by_core.resize(m_topology.get_counts().core);
	m_rthreads_by_core.resize(m_topology.get_counts().core);
}

static std::optional<uint64_t> my_stoll(const std::string &s)
{
	char* p;
	uint64_t n = ::strtoll(s.c_str(), &p, 10);

	if (*p != '\0')
		return {};

	return n;
}

std::map<uint64_t, ProcFS::ProcRecord> ProcFS::read_procfs() const
{
	std::map<uint64_t, ProcRecord> procs;

	for (const auto &d_it : fs::directory_iterator(procfs_mount)) {
		std::string d = d_it.path().filename();

		std::optional<uint64_t> p = my_stoll(d);
		if (!p)
			continue;

		ProcRecord rec = {0};
		rec.pid = *p;

		if (!rec.read_children())
			continue;

		if (!rec.read())
			continue;

		rec.core_id = m_topology[rec.cpu_id].core;
		procs[*p] = rec;

		for (auto &cpid : rec.children) {
			ProcRecord crec = {0};
			crec.pid = cpid;

			if (!crec.read())
				continue;

			crec.core_id = m_topology[crec.cpu_id].core;
			procs[cpid] = crec;
		}
	}

	return procs;
}

uint64_t ProcFS::cpu_to_core_id(uint64_t cpu) const
{
	return cpu / 2;
}

void ProcFS::update()
{
	std::map<uint64_t, ProcRecord> procs_new = read_procfs();

	m_procs_delta.clear();

	for (const auto &[pid, proc] : procs_new) {
		auto p_it = m_procs_old.find(pid);
		if (p_it == m_procs_old.end())
			continue;

		const auto &p = p_it->second;
		ProcRecord r = proc;
		r.utime -= p.utime;
		r.stime -= p.stime;
		r.rchar -= p.rchar;
		r.wchar -= p.wchar;
		r.read_bytes -= p.read_bytes;
		r.write_bytes -= p.write_bytes;

		m_procs_delta[pid] = r;
	}

	std::fill(m_cputime_by_core.begin(), m_cputime_by_core.end(), 0);
	std::fill(m_rthreads_by_core.begin(), m_rthreads_by_core.end(), 0);

	for (auto &[i, r] : m_procs_delta) {
		m_cputime_by_core[r.core_id] += r.utime + r.stime;

		if (r.state == 'R')
			m_rthreads_by_core[r.core_id]++;
	}

	for (auto &[i, r] : m_procs_delta) {
		if (m_cputime_by_core[r.core_id] == 0)
			r.coretime = 0;
		else
			r.coretime = 1. * (r.utime + r.stime) / m_cputime_by_core[r.core_id];

		if (m_rthreads_by_core[r.core_id] == 0)
			r.corefrac = 0;
		else
			r.corefrac = 1. / m_rthreads_by_core[r.core_id];
	}

	m_procs_old = procs_new;
}

void ProcFS::count_proc(uint64_t pid, Record &record) const
{
	auto p_it = m_procs_delta.find(pid);
	if (p_it == m_procs_delta.end())
		return;

	auto r = p_it->second;
	record.inc(Record::Threads);

	if (r.state == 'R') {
		record.inc(Record::RunningThreads);
		record.set_cpumask_bit(r.cpu_id);
		record.inc(Record::CoreFrac, r.corefrac);
	}

	record.inc(Record::UserTime, r.utime);
	record.inc(Record::SystemTime, r.stime);
	record.inc(Record::CoreTime, r.coretime);

	record.inc(Record::WriteChars, r.wchar);
	record.inc(Record::ReadChars, r.rchar);

	record.inc(Record::StorageWrite, r.write_bytes);
	record.inc(Record::StorageRead, r.read_bytes);

	record.inc(Record::MemoryVirtual, r.mem_size);
	record.inc(Record::MemoryResident, r.mem_resident);
	record.inc(Record::MemoryShared, r.mem_shared);

	for (uint64_t c: r.children)
		count_proc(c, record);
}

bool ProcFS::ProcRecord::read_children()
{
	fs::path taskdir = procfs_mount / std::to_string(pid) / "task";

	if (!fs::is_directory(taskdir))
		return false;

	for (const auto &d_it : fs::directory_iterator(taskdir)) {
		auto tid = d_it.path().filename();

		auto t = my_stoll(tid);
		if (!t)
			continue;

		if (t != pid) {
			children.insert(*t);
			continue;
		}

		std::stringstream content;

		try {
			std::ifstream in(d_it.path() / "children");
			content << in.rdbuf();
		} catch (...) {
			return false;
		}

		uint64_t p;
		while (content >> p)
			children.insert(p);
	}

	return true;
}

bool ProcFS::ProcRecord::read_stats()
{
	fs::path p = procfs_mount / std::to_string(pid) / "stat";

	std::string content;

	{
		std::ifstream in(p);
		content = std::string(
			std::istreambuf_iterator<char>(in),
			std::istreambuf_iterator<char>()
		);
	}

	if (content.empty())
		return false;

	size_t pos = content.find_last_of(')');

	if (pos == std::string::npos)
		return false;

	pos++;

	for (size_t i = 3; i < 40; i++) {
		pos++;

		size_t n = content.find(' ', pos);
		if (n == std::string::npos)
			return false;

		const std::string s = content.substr(pos, n - pos);

		if (i == 3)
			state = s[0];
		if (i == 14)
			utime = std::stoull(s);
		if (i == 15)
			stime = std::stoull(s);
		if (i == 39)
			cpu_id = std::stoull(s);

		pos = n;
	}

	return true;
}

bool ProcFS::ProcRecord::read_mem()
{
	fs::path p = procfs_mount / std::to_string(pid) / "statm";

	std::stringstream content;

	try {
		std::ifstream in(p);
		content << in.rdbuf();
	} catch (...) {
		return false;
	}

	content >> mem_size;
	content >> mem_resident;
	content >> mem_shared;

	return true;
}

bool ProcFS::ProcRecord::read_io()
{
	fs::path p = procfs_mount / std::to_string(pid) / "io";

	std::stringstream content;

	try {
		std::ifstream in(p);
		content << in.rdbuf();
	} catch (...) {
		return false;
	}

	std::string key;
	std::string value;

	while (content >> key >> value) {
		uint64_t v = std::stoll(value);
		if (key == "rchar:")
			rchar = v;
		if (key == "wchar:")
			wchar = v;
		if (key == "read_bytes:")
			read_bytes = v;
		if (key == "write_bytes:")
			write_bytes = v;
	}

	return true;
}

bool ProcFS::ProcRecord::read()
{
	if (!read_stats())
		return false;
	if (!read_mem())
		return false;
	if (!read_io())
		return false;
	return true;
}


