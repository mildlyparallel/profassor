#include <cstring>
#include <iostream>
#include <cassert>
#include <filesystem>

#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>

#include "Process.hh"
#include "Utils.hh"
#include "Config.hh"

Process::Process()
: m_logger("process")
{ }

Process::~Process()
{ 
	if (m_pid == -1)
		return;

	int rc = ::kill(-m_pid, SIGKILL);
	if (rc < 0)
		return;

	int st;
	pid_t p;

	do {
		p = ::waitpid(m_pid, &st, WNOHANG);
	} while (p <= -1 && errno == EINTR);
}

void Process::set_file(const std::string &file)
{
	m_file = file;
}

void Process::set_args(const std::vector<std::string> &args)
{
	m_args = args;
}

pid_t Process::fork()
{
	assert(m_pid <= 0);

	m_logger.debug("About to fork", m_file);

	int rc;

	m_pipe_fd[0] = 0;
	m_pipe_fd[1] = 0;
	rc = pipe(m_pipe_fd);
	THROW_IF(rc < 0);

	m_pid = ::fork();
	THROW_IF(m_pid < 0);

	if (m_pid > 0)
		return m_pid;

	auto argv = args_to_argv();

	reset_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT, SIGCHLD});

	rc = setpgid(0, 0);
	THROW_IF(rc < 0);

	pipe_read_and_close();

	rc = ::execvp(m_file.c_str(), argv);
	THROW_IF(rc < 0);

	return -1;
}

void Process::exec()
{
	pipe_write_and_close();
}

void Process::pipe_read_and_close()
{
	int rc = 0;

	do {
		int proceed = 0;
		rc = ::read(m_pipe_fd[0], &proceed, sizeof(int));
	} while (rc < 0 && errno == EINTR);
	THROW_IF(rc < 0);

	rc = ::close(m_pipe_fd[0]);
	THROW_IF(rc < 0);

	rc = ::close(m_pipe_fd[1]);
	THROW_IF(rc < 0);
}

void Process::pipe_write_and_close()
{
	int rc = 0;

	do {
		int proceed = 1;
		rc = ::write(m_pipe_fd[1], &proceed, sizeof(int));
	} while (rc < 0 && errno == EINTR);
	THROW_IF(rc < 0);

	rc = ::close(m_pipe_fd[0]);
	THROW_IF(rc < 0);

	rc = ::close(m_pipe_fd[1]);
	THROW_IF(rc < 0);
}

void Process::reset_signals(std::initializer_list<uint32_t> signals)
{
	int rc = 0;

	sigset_t mask;
	rc = ::sigemptyset(&mask);
	THROW_IF(rc < 0);
	for (auto s : signals) {
		rc = ::sigaddset(&mask, s);
		THROW_IF(rc < 0);
	}

	rc = ::sigprocmask(SIG_UNBLOCK, &mask, NULL);
	THROW_IF(rc < 0);
}

char * const *Process::args_to_argv() const
{
	char const* *argv = new char const *[m_args.size() + 2]();

	argv[0] = m_file.c_str();

	for (size_t i = 0; i < m_args.size(); ++i)
		argv[i+1] = m_args[i].c_str();

	argv[m_args.size()+1] = NULL;

	return (char * const *)(argv);
}

bool Process::sync()
{
	assert(m_pid > 0);

	int st;
	pid_t p;

	do {
		p = ::waitpid(m_pid, &st, WNOHANG);
		THROW_IF(p <= -1 && errno != EINTR);
		// XXX: child may double-fork and errno would be ECHILD
	} while (p <= -1 && errno == EINTR);

	if (p == 0)
		return false;

	if (WIFEXITED(st)) {
		m_state = Exited;
		m_return_code = WEXITSTATUS(st);

		m_logger.info("PID", m_pid, "exited with return code", m_return_code);
	} else {
		m_state = Terminated;
		m_logger.info("PID", m_pid, "terminated");
	}

	m_pid = -1;

	return true;
}

void Process::stop(bool force)
{
	if (m_pid == -1)
		return;

	if (force) {
		int rc = ::kill(-m_pid, SIGKILL);
		THROW_IF(rc < 0);
	} else {
		int rc = ::kill(-m_pid, SIGINT);
		THROW_IF(rc < 0);
	}
}

