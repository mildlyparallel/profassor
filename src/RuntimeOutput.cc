#include "RuntimeOutput.hh"

#include "Record.hh"

RuntimeOutput::RuntimeOutput(const std::string &p)
: m_logger("output")
, m_out(p)
{
	m_logger.info("Writing runtime values to", p);
}


void RuntimeOutput::write(const Record &record)
{
	if (!m_written_header) {
		write_header();
		m_written_header = true;
	}

	bool comma = false;

	for (size_t i = 0; i < Record::nr_values; ++i) {
		if (comma)
			m_out << ",";
		comma = true;

		if (i == Record::RunningThreadsCpumask) {
			m_out << record.get_cpumask_string();
		} else if (i == Record::CoreTime || i == Record::CoreFrac) {
			m_out << record[i].dbl;
		} else {
			m_out << record[i].u64;
		}
	}

	m_out << "\n";

	m_out.flush();
}

void RuntimeOutput::write_header()
{
	bool comma = false;

	for (size_t i = 0; i < Record::nr_values; ++i) {
		if (comma)
			m_out << ",";
		comma = true;

		m_out << Record::get_name(i);
	}

	m_out << "\n";
}

