# profassor

**profassor** Linux process profiler

*by Ruslan Kuchumov, 2022*

# Compiling

Make sure you have [meson](http://mesonbuild.com/Getting-meson.html) and
[ninja](https://ninja-build.org/) build systems installed.

To compile and install *profassor* call:

```bash
mkdir build && cd build
meson ..
ninja
sudo ninja install
```

# Usage

*profassor* starts your command in subprocess and monitors its hardware (perf)
and software (procfs) counters.

For example, the following command would start `stress -c 3`:

```bash
profassor stress -c 3
```

It would create `stats.csv` file (can be changed with `-o` option) with runtime
counters values:

```bash
> cat stats.csv
Timestamp,Threads,RunningThreads,UserTime,SystemTime,WriteChars,ReadChars,StorageWrite,StorageRead,MemoryVirtual,MemoryResident,MemoryShared,Instructions,CpuCycles,CacheMisses,CacheReferences,RunningThreadsCpumask
246,4,4,60,184,684,52328,0,0,1563356,943958,3112,7477338,487905441,186934,6033933,e
494,4,3,219,76,0,0,0,0,1563356,1316000,3112,1853441099,1787251382,19974879,38185950,b
744,4,4,317,0,0,0,0,0,1564324,1318272,3308,3740016290,2577592322,40779025,421587716,f
...
```

# Counters description

Output file (`stats.csv` by default) has the following columns:

| Column                | Source                                            | Description                                     |
| ------                | ------                                            | ----------                                      |
| Timestamp             | std::chrono::steady_clock                         | Milliseconds since command start                |
| Threads               | /proc/[pid]/task, /proc/[pid]/task/[tid]/children | Number of child thread and processes            |
| RunningThreads        | /proc/[pid]/stat - state                          | Number of running children (R state)            |
| UserTime              | /proc/[pid]/stat - utime                          | User mode CPU time in clock ticks               |
| SystemTime            | /proc/[pid]/stat - stime                          | Kernel mode CPU time in clock ticks             |
| RunningThreadsCpumask | /proc/[pid]/stat - processor                      | Mask of last seen CPUs of running processes     |
| WriteChars            | /proc/[pid]/io - wchar                            | Number of bytes passed to *write()* system call |
| ReadChars             | /proc/[pid]/io - rchar                            | Number of bytes passed to *read()* system call  |
| StorageWrite          | /proc/[pid]/io - read_bytes                       | Number of read bytes from the storage layer     |
| StorageRead           | /proc/[pid]/io - write_bytes                      | Number of send bytes to the storage layer       |
| MemoryVirtual         | /proc/[pid]/statm - size                          | Virtual memory size in bytes                    |
| MemoryResident        | /proc/[pid]/statm - resident                      | Resident set size in bytes                      |
| MemoryShared          | /proc/[pid]/statm - shared                        | Number of resident shared pages                 |
| Instructions          | PERF_COUNT_HW_INSTRUCTIONS                        | Number of retired CPU instructions              |
| CpuCycles             | PERF_COUNT_HW_CPU_CYCLES                          | Number of CPU cycles                            |
| CacheMisses           | PERF_COUNT_HW_CACHE_MISSES                        | Number of cache misses                          |
| CacheReferences       | PERF_COUNT_HW_CACHE_REFERENCES                    | Number of cache accesses                        |

Refer to `man procfs` and `man perf_event_open` manual pages for further explanation.

> Note that values are not cumulative and they show change since the last sample

> Note that perf counters do not include kernel events.

