#pragma once

#include <array>
#include <cstdint>
#include <ostream>
#include <bitset>
#include <any>

#include "config.h"

class Record
{
public:

	union value_type {
		uint64_t u64;
		double dbl;
	};

	enum Value : size_t {
		Timestamp = 0,

		Threads,
		RunningThreads,

		UserTime,
		SystemTime,

		CoreTime,
		CoreFrac,

		WriteChars,
		ReadChars,

		StorageWrite,
		StorageRead,

		MemoryVirtual,
		MemoryResident,
		MemoryShared,

		Instructions,
		CpuCycles,
		CacheMisses,
		CacheReferences,
		// BusAccess,
		// BusCycles,
		// MemLoad,
		// MemStore,
		// StallFrontend,
		// StallBackend,

		RunningThreadsCpumask,

		nr_values,
	};

	Record();

	void reset();

	value_type &get(Value v);

	value_type get(Value v) const;

	value_type &operator[](Value v);

	value_type operator[](Value v) const;

	value_type &operator[](size_t i);

	value_type operator[](size_t i) const;
	
	void inc(Value v, uint64_t x = 1ull);

	void inc(Value v, double x);

	static const char *get_name(size_t i);

	static const char *get_name(Value v);

	void set_cpumask_bit(size_t cpu, bool value = true);

	std::string get_cpumask_string() const;

protected:

private:
	static constexpr const char *m_names[Record::Value::nr_values] = {
		"Timestamp",

		"Threads",
		"RunningThreads",

		"UserTime",
		"SystemTime",

		"CoreTime",
		"CoreFrac",

		"WriteChars",
		"ReadChars",

		"StorageWrite",
		"StorageRead",

		"MemoryVirtual",
		"MemoryResident",
		"MemoryShared",

		"Instructions",
		"CpuCycles",
		"CacheMisses",
		"CacheReferences",
		// "BusAccess",
		// "BusCycles",
		// "MemLoad",
		// "MemStore",
		// "StallFrontend",
		// "StallBackend",

		"RunningThreadsCpumask"
	};

	std::array<value_type, nr_values> m_values;

	std::bitset<MAX_CPUS> m_cpuset;
};

