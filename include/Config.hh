#pragma once

#include <vector>
#include <chrono>

#include "Record.hh"

#include <linux/perf_event.h>

class Config
{
public:
	struct PerfEvent {
		Record::Value name;
		uint32_t type;
		uint32_t config;
	};

	static constexpr PerfEvent perf_events[] = {
		{Record::Instructions, PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS},
		{Record::CpuCycles, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CPU_CYCLES},
		{Record::CacheReferences, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_REFERENCES},
		{Record::CacheMisses, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_MISSES},
		// {Record::BusCycles, PERF_TYPE_HARDWARE, PERF_COUNT_HW_BUS_CYCLES},
		// {Record::MemLoad, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_NODE | (PERF_COUNT_HW_CACHE_OP_READ << 8) | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)},
		// {Record::MemStore, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_NODE | (PERF_COUNT_HW_CACHE_OP_WRITE << 8) | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)},
		// {Record::StallFrontend, PERF_TYPE_HARDWARE, PERF_COUNT_HW_STALLED_CYCLES_FRONTEND},
		// {Record::StallBackend, PERF_TYPE_HARDWARE, PERF_COUNT_HW_STALLED_CYCLES_BACKEND}
		// {Record::MemStore, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CPU_CYCLES},
		// {Record::BusAccess, PERF_TYPE_RAW, 0x19},
	};

	void parse_cmdline(int argc, const char *argv[]);

	bool verbose = false;

	std::string command_path;

	std::vector<std::string> command_args;

	std::chrono::milliseconds sample_interval = std::chrono::milliseconds(250);

	std::string runtime_output_file = "stats.csv";
};


