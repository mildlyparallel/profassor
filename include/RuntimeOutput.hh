#pragma once

#include <string>
#include <fstream>

#include "Logger.hh"
#include "Record.hh"

class RuntimeOutput
{
public:
	RuntimeOutput(const std::string &p);

	void push(const Record &record);

	void write(const Record &record);

protected:

private:
	void write_header();

	Logger m_logger;

	bool m_written_header = false;

	std::ofstream m_out;
};
