#pragma once

#include <cstdint>

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define THROW_IF(cond) {\
	if ((cond)) \
		throw std::system_error(errno, std::generic_category(), __FILE__ ":" STR(__LINE__)); \
} while(false);

