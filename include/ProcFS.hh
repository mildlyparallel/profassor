#pragma once

#include <filesystem>
#include <set>
#include <map>
#include <vector>

#include "Logger.hh"
#include "Record.hh"
#include "Topology.hh"

class ProcFS
{
public:
	ProcFS();

	void set_pid(uint64_t pid);

	void update();

	void count_proc(uint64_t pid, Record &record) const;

protected:

private:
	inline
	static const std::filesystem::path procfs_mount ="/proc";

	struct ProcRecord {
		uint64_t pid;
		char state;
		uint64_t utime;
		uint64_t stime;
		double coretime;
		double corefrac;

		uint64_t cpu_id;
		uint64_t core_id;

		uint64_t rchar;
		uint64_t wchar;
		uint64_t read_bytes;
		uint64_t write_bytes;

		uint64_t mem_size;
		uint64_t mem_resident;
		uint64_t mem_shared;

		std::set<uint64_t> children;

		bool read_children();

		bool read();

		bool read_stats();

		bool read_mem();

		bool read_io();

	};

	uint64_t cpu_to_core_id(uint64_t cpu) const;

	std::map<uint64_t, ProcRecord> read_procfs() const;

	Logger m_logger;

	Topology m_topology;

	std::map<uint64_t, ProcRecord> m_procs_old;
	std::map<uint64_t, ProcRecord> m_procs_delta;

	std::vector<uint64_t> m_cputime_by_core;
	std::vector<uint64_t> m_rthreads_by_core;
};

