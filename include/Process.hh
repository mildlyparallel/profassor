#pragma once

#include <string>
#include <vector>
#include <initializer_list>

#include <sys/types.h>

#include "Config.hh"
#include "Logger.hh"

class Process
{
public:
	enum State : uint8_t {
		Empty = 0,
		Pending,
		Spawned,
		Exited,
		Terminated,
		Failed,

		nr_states
	};

	Process();

	~Process();

	void set_file(const std::string &file);

	void set_args(const std::vector<std::string> &args);

	pid_t fork();

	void exec();

	bool sync();

	void stop(bool force = false);

private:
	char * const *args_to_argv() const;

	void pipe_read_and_close();

	void pipe_write_and_close();

	void reset_signals(std::initializer_list<uint32_t> signals);

	int m_pipe_fd[2] = {0};

	std::string m_file;

	std::vector<std::string> m_args;

	State m_state;

	int m_return_code = 0;

	pid_t m_pid = -1;

	Logger m_logger;
};


