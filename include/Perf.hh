#pragma once

#include <array>
#include <cstdint>

#include "Record.hh"
#include "Logger.hh"
#include "Config.hh"

class Perf
{
public:
	Perf();

	~Perf();

	void start_listening(int pid);

	void stop_listening();

	void update(Record &stats);

private:
	static constexpr size_t nr_events = std::size(Config::perf_events);

	bool add(int pid, uint32_t type, uint32_t config);

	void reset();

	void enable();

	void disable();

	void read();

	void close();

	struct EventFD {
		int fd;
		uint64_t id;
		uint64_t value;
	};

	size_t m_next_event_id;

	std::array<EventFD, nr_events> m_events;

	Logger m_logger;
};


