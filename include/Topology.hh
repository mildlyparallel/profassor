#pragma once

#include <memory>
#include <vector>

struct hwloc_obj;
struct hwloc_topology;

typedef struct hwloc_topology* hwloc_topology_t;
typedef struct hwloc_obj* hwloc_obj_t;

class Topology
{
public:
	Topology();

	~Topology();

	struct Processor {
		size_t machine = 0;
		size_t numanode = 0;
		size_t package = 0;
		size_t core = 0;
		size_t cpu = 0;
	};

	using container_type = std::vector<Processor>;

	using const_iterator = container_type::const_iterator;

	inline 
	const_iterator begin() const {
		return m_processors.begin();
	}

	inline 
	const_iterator end() const {
		return m_processors.end();
	}

	inline 
	const Processor &get(size_t id) const {
		return m_processors[id];
	}

	inline
	const Processor &operator[](size_t id) const {
		return get(id);
	}

	inline
	size_t size() const {
		return m_processors.size();
	}

	inline
	const Processor &get_counts() const {
		return m_counts;
	}

protected:

private:
	void load();

	void load_recursive(hwloc_obj_t obj, Processor processor);

	std::unique_ptr<hwloc_topology_t> m_topo;

	container_type m_processors;

	Processor m_counts;
};
