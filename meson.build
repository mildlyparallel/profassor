project(
	'Profassor',
	'cpp',
	version: '1.0.0',
	default_options : [
	  'buildtype=plain',
	  'cpp_std=gnu++2a'
	]
)

config = configuration_data()
config.set('version', meson.project_version())

prfs_targets = [
	['profassor', 'src/profassor.cc'],
]

prfs_sources = [
	'src/Config.cc',
	'src/Perf.cc',
	'src/Logger.cc',
	'src/Record.cc',
	'src/Process.cc',
	'src/Poll.cc',
	'src/RuntimeOutput.cc',
	'src/ProcFS.cc',
	'src/Topology.cc',
]

incdir = [
	include_directories('include')
]

cpp = meson.get_compiler('cpp')


cxxflags = [ ]

if get_option('buildtype') == 'release'
	cxxflags += '-DNDEBUG=1'
endif

test_flags = [  ]

foreach flag : test_flags
	if cpp.has_argument(flag)
		test_flags += flag
	endif
endforeach

deps = [ ]

dashh_dep = dependency('dashh', required : false)

if not dashh_dep.found()
	dashh_subporject = subproject('dashh',
		default_options : [
			'build-as-subproject=true',
		]
	)
	dashh_dep = dashh_subporject.get_variable('dashh_dep')
endif

deps += dashh_dep

hwloc_dep = dependency('hwloc', required : true)

deps += hwloc_dep

test_functions = [ ]

foreach func : test_functions
	if cpp.has_function(func)
		config.set('HAVE_' + func.underscorify().to_upper(), true)
	endif
endforeach

configure_file(
	input : 'config.h.in',
	output : 'config.h',
	configuration : config
)

prfs_library = static_library(
	'mp',
	sources : prfs_sources,
	include_directories : incdir,
	dependencies : deps,
	c_args : cxxflags
)

foreach t : prfs_targets
	executable(
		t[0],
		sources : t[1],
		include_directories : incdir,
		link_with : prfs_library,
		dependencies : deps,
		cpp_args : cxxflags,
		install: true
	)
endforeach

